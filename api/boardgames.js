'use strict'

const redis = require('redis').createClient(process.env.REDIS_PORT || 6379, process.env.REDIS_HOST || '127.0.0.1');
redis.on("error", function (err) {
  console.error("Redis error: %s", err);
});
const reds = require('reds');
reds.client = redis;
const imageBase = process.env.IMAGE_BASE;
const imageExtension = process.env.IMAGE_EXTENSION || '.jpg';

const search = reds.createSearch('{boardgame:search:name}');

function _find(param, cb) {
  if (param.name) {
    _findByName(param.name, cb);
  } else if (param.ids) {
    _findByIds(param.ids, cb);
  } else {
    redis.zrangebyscore('boardgames', param.offset + 1, param.offset + param.limit, function (err, boardgameids) {
      if (err) cb(err);
      else _findByIds(boardgameids, cb);
    });
  }
}

function _findByName(name, cb) {
  search
    .query(name)
    .end(function (err, boardgameids) {
      if (err) cb(err);
      else if (boardgameids.length === 0) cb(null, []);
      else {
        _findByIds(boardgameids, function (err, boardgames) {
          if (err) cb(err);
          else cb(null, boardgames.sort(_sort));
        });
      }
    });
}

function _findByIds(ids, cb) {
  redis.mget(ids.map(id => '{boardgame}:' + id), function (err, boardgames) {
    if (err) cb(err);
    else cb(null, boardgames.map(_parse).filter(bg => { return !!bg; }));
  });
}

function _findById(id, cb) {
  redis.get('{boardgame}:' + id, function (err, bg) {
    cb(err, _parse(bg));
  });
}

function _parse(bgstr) {
  var bgobj = JSON.parse(bgstr);
  if (bgobj) {
    bgobj.image = imageBase ? 'http://' + imageBase + '/' + bgobj.id + '_t' + imageExtension : undefined;
  };
  return bgobj;
}

function _sort(bg1, bg2) {
  return bg1.rank - bg2.rank;
}

module.exports = {
  find: _find,
  findById: _findById
};
