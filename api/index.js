'use strict'

const express = require('express');
const compression = require('compression');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');
const app = express();
const Boardgames = require('./boardgames');

app.use(compression());
app.use(allowAllCors);
app.use(awsServerlessExpressMiddleware.eventContext());
app.use(errorHandler);

app.get('/boardgame', (req, res) => {
  var name = req.query.name && (req.query.name + '').length > 2 ? req.query.name : undefined;
  var ids = req.query.id ? req.query.id.split(',') : undefined;
  var offset = typeof req.query.offset === 'number' && req.query.offset >= 0 ? req.query.offset : 0;
  var limit = typeof req.query.limit === 'number' && req.query.limit >= 1 && req.query.limit <= 100 ? req.query.limit : 100;

  Boardgames.find({name: name, ids: ids, offset: offset, limit: limit}, function (err, boardgames) {
    if (err) res.send(err);
    else res.json({data: boardgames, metadata: {count: boardgames.length, offset: offset, limit: limit}});
  });
});

app.get('/boardgame/:id', (req, res) => {
  Boardgames.findById(req.params.id, function (err, boardgame) {
    if (err) res.send(err);
    else res.json(boardgame)
  });
});

app.get('/', (req, res) => {
  res.send('Thanks for coming!!');
})

function allowAllCors(req, res, next) {
  // see: http://stackoverflow.com/questions/7067966/how-to-allow-cors-in-express-nodejs
  res.header('Access-Control-Allow-Origin', req.headers.origin);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  if (req.method === "OPTIONS") {
    res.send(200);
  } else {
    next();
  }
}

function errorHandler(err, req, res, next) {
  console.log(err.stack);
  res.status(500).send({error: err.stack});
}

console.log('listening to port 3001');
app.listen(3001)

module.exports = app;
