# boardgame
[![pipeline status](https://gitlab.com/duduksini/boardgame/badges/master/pipeline.svg)](https://gitlab.com/duduksini/boardgame/commits/master)  

boardgame is a master data of 33k+ most popular boardgames.  

More project info in the [wiki](https://gitlab.com/duduksini/boardgame/wikis/boardgame)

## Push a tag to trigger release
* See existing tags `git tag`
* Bump version in **/api/package.json**
* `git tag -a *.*.* -m "Release description"`
* `git push --tags`

## Deployment to AWS
New tags trigger deployment to AWS via cloudformation template in **/aws**
* **/api** will be deployed as Lambda
* **/db** will be deployed as a Redis cluster in Elasicache
* **/img** will be deployed as Cloudfront distribution serving S3

## Running locally
* Make sure [docker](https://www.docker.com/community-edition) is running  
* In **/local**, Run `docker-compose up --build --force-recreate --remove-orphans`  
* Verify [http://localhost:3001/boardgame](http://localhost:3001/boardgame)  
