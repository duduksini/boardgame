#!/bin/bash -ex

Template=${1:-cf.yml}
StackName=${2:-duduksini-boardgame}

aws cloudformation validate-template --template-body file://$Template

aws cloudformation package \
  --template $Template \
  --output-template-file cf.pkg.yml \
  --s3-bucket duduksini \
  --s3-prefix boardgame/api \
  --force-upload

aws cloudformation deploy \
  --stack-name $StackName \
  --template-file cf.pkg.yml \
  --capabilities CAPABILITY_IAM \
  --no-fail-on-empty-changeset
