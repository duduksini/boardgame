#!/bin/bash -ex

File=${1:-../img/img.zip}
S3Path=${2:-duduksini/boardgame/img/}

mkdir tmp && unzip -oq $File -d tmp
aws s3 sync tmp s3://$S3Path --acl public-read --sse AES256
rm -rf tmp
