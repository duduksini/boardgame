#!/bin/bash -ex

File=${1:-../db/db.rdb}
S3Path=${2:-duduksini/boardgame/db/seed/}

aws s3 cp $File s3://$S3Path --acl public-read --sse AES256
